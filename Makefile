# -*- Mode: BSDmakefile; -*-                                             
#
# Makefile<maxwell>
#

# We use -fno-sanitize-recover so the program will stop when undefined
# behavior is encountered, instead of simply printing a warning and
# continuing.
ASAN_FLAGS= -fsanitize=address,undefined -fno-sanitize-recover=all

CWARNS = -Wall -Wextra \
	-Wcast-align \
	-Wcast-qual \
	-Wdisabled-optimization \
	-Wpointer-arith \
	-Wshadow \
	-Wsign-compare \
	-Wundef \
	-Wwrite-strings \
	-Wbad-function-cast \
	-Wmissing-prototypes \
	-Wnested-externs \
	-Wstrict-prototypes \
        -Wunreachable-code \
        -Wno-missing-declarations

CFLAGS= --std=c99 $(ASAN_FLAGS) $(CWARNS)

all: unittest maxwell.o

maxwell.o: maxwell.c maxwell.h
	$(CC) $(CFLAGS) -c -o $@ maxwell.c

unittest: unittest.c unittest.h maxwell.o
	$(CC) $(CFLAGS) -o $@ $< maxwell.o

clean:
	@rm -rf *.o *.dSYM unittest

tags: *.[ch]
	@if [ -z $(shell which etags) ]; then \
	echo "etags not found, skipping TAGS file update"; \
	else etags -o TAGS *.[ch]; \
	fi

