/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  unittest.h                                                              */
/*                                                                          */
/*  AUTHORS: Jamie A. Jennings                                              */

#ifndef unittest_h
#define unittest_h

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>		/* for logging/tracing */
#include <stdarg.h>		/* for logging/tracing */
#include <stdlib.h>		/* for malloc/free */

#include "maxwell.h"

#endif
