/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  unittest.c                                                              */
/*                                                                          */
/*  AUTHORS: Jamie A. Jennings                                              */

#include "unittest.h"
#include <assert.h>

#define TORNIL(b) (b ? T : NIL)

/* ----------------------------------------------------------------------------- */
/* TESTING                                                                       */
/* ----------------------------------------------------------------------------- */

static int test_tag (value_t v, value_types t) {
  bool ok = (TYPE_OF(v) == t);
  if (ok) {
    printf("PASSED: value has type tag %lld, %s (AS EXPECTED)\n",
	   TYPE_OF(v), typename(TYPE_OF(v)));
  } else {
    printf("ERROR:  value has type tag %lld, %s -- expected %d, %s\n",
	   TYPE_OF(v), typename(TYPE_OF(v)),
	   t, typename(t));
    print_value_tags(v);
  }
  return ok;
}

#define PASSFAIL(b) ((b) ? "PASS" : "FAIL")

static bool eqtest (const char *desc, value_t a, value_t b, value_t expected_value) {
  value_t result_value = TORNIL(eq(a, b));
  printf("%s: %s (eq %s %s) ==> %s\n",
	 PASSFAIL(result_value == expected_value),
	 desc,
	 tostring(a), tostring(b), tostring(result_value));
  return (result_value == expected_value);
}

static bool equaltest (value_t a, value_t b, value_t expected_value) {
  value_t result_value = TORNIL(equal(a, b));
  printf("%s: (equal %s %s) ==> %s\n",
	 PASSFAIL(result_value == expected_value),
	 tostring(a), tostring(b), tostring(result_value));
  return (result_value == expected_value);
}

static bool evaltest (const char *desc,
	       value_t exp,
	       value_t expected_value,
	       bool expected_match,
	       value_t env) {
  value_t value = eval(exp, env);
  bool matched = equal(value, expected_value);
  bool passed_test = expected_match ? matched : !matched;
  printf("%s: %s (eval %s) ==> %s, (eq %s %s) ==> %s\n",
	 PASSFAIL(passed_test),
	 desc,
	 tostring(exp), tostring(value),
	 tostring(value), tostring(expected_value),
	 tostring(TORNIL(matched)));
  return (expected_match == matched);
}  

static bool applytest (value_t operator, value_t operands, value_t expected_value, value_t env) {
  value_t value = apply(operator, operands, env);
  bool matched = equal(value, expected_value);
  printf("%s: (apply %s %s) ==> %s, expected %s\n",
	 PASSFAIL(matched),
	 tostring(operator), tostring(operands),
	 tostring(value), tostring(expected_value));
  return matched;
}  

#define TF(b) (((b)==NIL) ? "FALSE" : ((b != INVALID) ? "TRUE" : "INVALID"))

static bool atomtest (const char *desc, value_t exp, value_t expected_result) {
  value_t result = TORNIL(atom(exp));
  printf("%s: %s (atom %s) ==> %s, expected %s\n",
	 PASSFAIL(result == expected_result),
	 desc, 
	 tostring(exp),
	 TF(result),
	 TF(expected_result));
  return (expected_result == result);
}
  
/* ----------------------------------------------------------------------------- */
/* MAIN: Unit tests                                                              */
/* ----------------------------------------------------------------------------- */

int main (void) {

  value_t ENV = NIL;
  assert(symbol_table == NULL);
  assert(heap == NULL);

  CONTINUE_AFTER_PANIC = true;		/* for unit testing */

  printf("\n-------------------------------------------- Symbol table testing\n");

  symbol_table = new_symbol_table(ELEMENTARY_SYMBOLS_COUNT - 1);
  assert(!symbol_table);	/* not enough room for elementary symbols */

  symbol_table = new_symbol_table(ELEMENTARY_SYMBOLS_COUNT);
  dump_symbol_table();

  symbol_table = new_symbol_table(ELEMENTARY_SYMBOLS_COUNT + 10);

  symbol_t tempsymnum;
  char tempsymname[] = "tempX";
  tempsymname[4] = 'A';
  while ((tempsymnum = symbol_intern(tempsymname)) >= 0) {
    tempsymname[4]++;
  }
  dump_symbol_table();
  assert(symbol_table->next == symbol_table->capacity);

  if (!((heap = new_heap(30)))) {
    printf("ERROR initializing heap!  Exiting...\n");
    exit(-1);
  }

  printf("\n-------------------------------------------- Tagged value testing\n");
  printf("Type tags: (there are %d)\n", TYPE_COUNT);
  for (int i=0; i<TYPE_COUNT; i++)
    printf("  %2d %s\n", i, typename(i));

  printf("MASK_FOR_GC: %016llx\n", MASK_FOR_GC);
  printf("MASK_FOR_TYPE: %016llx\n", MASK_FOR_TYPE);
  printf("MASK_FOR_VALUE: %016llx\n", MASK_FOR_VALUE);

  value_t dummy = 0;
  for (int i=0; i<TYPE_COUNT; i++) {
    dummy = 0;
    printf("Setting type to %s (%d)\n", typename(i), i);
    dummy = TO_VALUE_T(dummy, i);
    printf("value: ");
    print_value_as_value(dummy);
    puts("");
    print_value_tags(dummy);
  }

  const value_t quote =   (value_t) TO_VALUE_T(0L, TYPE_SYMBOL);
    
  value_t invalid = INVALID;
  value_t one = from_fixnum(1);
  value_t negative_nine = from_fixnum(-9);
  value_t max = from_fixnum(FIXNUM_MAX);
  value_t min = from_fixnum(FIXNUM_MIN);
  
  test_tag(invalid, TYPE_INVALID);
  test_tag(one, TYPE_FIXNUM);
  test_tag(negative_nine, TYPE_FIXNUM);
  test_tag(max, TYPE_FIXNUM);
  test_tag(min, TYPE_FIXNUM);
  test_tag(NIL, TYPE_SYMBOL);
  test_tag(T, TYPE_SYMBOL);
  
  printf("invalid: %s\n", tostring(invalid)); /* leak */
  printf("nil: %s\n", tostring(NIL));	    /* leak */
  printf("one: %s\n", tostring(one));	    /* leak */
  printf("negative nine: %s\n", tostring(negative_nine));	/* leak */
  printf("min: %s\n", tostring(min));	    /* leak */
  printf("max: %s\n", tostring(max));	    /* leak */

  assert(from_fixnum(FIXNUM_MIN-1) == INVALID);
  assert(from_fixnum(FIXNUM_MAX+1) == INVALID);
  assert(from_fixnum(INT64_MAX) == INVALID);
  assert(from_fixnum(INT64_MIN) == INVALID);

  printf("\n-------------------------------------------- Store testing\n");

  dump_heap(0, 5);
  puts("");
  
  assert(store_value(from_fixnum(-999)) == 0);   /* FIRST STORE */
  assert(store_value_at(7, one) == 7);
  assert(store_value_at(7, negative_nine) == 7); /* overwrite locative */
  assert(load_value(7) == negative_nine);
  dump_heap(0, 10);

  value_t sym1 = from_symbol(MAX_SYMBOL-1);
  assert(sym1 != INVALID);
  value_t sym2 = from_symbol(55);

  locative_t loc = store_value(sym1);
  assert(loc != -1);
  assert(load_value(loc) == sym1);
  loc = store_value(sym2);
  assert(loc != -1);
  assert(load_value(loc) == sym2);

  locative_t max_loc = store_value(from_fixnum(FIXNUM_MAX));
  assert(to_fixnum(load_value(max_loc)) == FIXNUM_MAX);
  locative_t min_loc = store_value(from_fixnum(FIXNUM_MIN));
  assert(to_fixnum(load_value(min_loc)) == FIXNUM_MIN);

  dump_heap(0, 10);

  printf("Filling heap up to obstacle value we stored earlier...\n");
  loc = store_value(one);
  while (loc != -1) loc = store_value(one);
  dump_heap(-1, -1);

  printf("Reinitializing heap...\n");
  if (!((heap = new_heap(30)))) {
    printf("ERROR initializing heap!  Exiting...\n");
    exit(-1);
  }

  printf("Filling heap...\n");
  loc = store_value(one);
  while (loc != -1) loc = store_value(one);
  dump_heap(0, 5);
  dump_heap(heap->capacity - 5, -1);
  
  printf("\n-------------------------------------------- Symbol testing\n");
  value_t s5 = from_symbol(5);
  printf("symbol 5: %s\n", tostring(s5));	    /* leak */
  print_value_tags(s5);

  assert(s5 != INVALID);

  value_t s0 = from_symbol(0);
  printf("symbol 0: %s\n", tostring(s0));	    /* leak */
  assert(s0 != INVALID);

  value_t smax = from_symbol(MAX_SYMBOL-1);
  printf("symbol max: %s\n", tostring(smax));	    /* leak */
  assert(smax != INVALID);
  /*
    Note: can't test from_symbol(MAX_SYMBOL + 1) because the
    argument overflows symbol_t, and the compiler rightly complains.
  */

  value_t serr = from_symbol(MAX_SYMBOL);
  assert(to_symbol(serr) == MAX_SYMBOL);
  serr = from_symbol(INT32_MIN);
  assert(serr == INVALID);
  serr = from_symbol(-1);
  assert(serr == INVALID);
  
  printf("\n-------------------------------------------- Cons testing\n");

  printf("Reinitializing heap...\n");
  if (!((heap = new_heap(30)))) {
    printf("ERROR initializing heap!  Exiting...\n");
    exit(-1);
  }
  ENV = NIL;

  value_t c1 = cons(T, T);
  assert(c1 != INVALID);
  print_value_tags(c1);
  printf("stored a cons cell at location %d\n", to_locative(c1));
  assert(to_locative(c1) == 0);
  locative_t gretzky_loc = store_value(from_fixnum(99));
  printf("from_locative(gretzky_loc) is %s\n", tostring(from_locative(gretzky_loc)));
  value_t c2 = cons(from_locative(gretzky_loc), from_symbol(1));
  print_value_tags(c2);
  printf("stored a cons cell at location %d\n", to_locative(c2));
  assert(to_locative(c2) == 3);

  printf("\n-------------------------------------------- Car/Cdr testing\n");

  printf("Using heap produced by cons testing\n");
  assert(car(c1) == T);
  assert(cdr(c1) == T);
  printf("c2 is %016llX\n", (uint64_t) c2);
  printf("c2 value is %016llX\n", (uint64_t) VALUE_OF(c2));
  printf("car(c2) is %s\n", tostring(car(c2)));
  printf("cdr(c2) is %s\n", tostring(cdr(c2)));
  printf("lv is %s\n", tostring(load_value(1)));
  assert(to_locative(car(c2)) == gretzky_loc);

  assert(cdr(c2) == from_symbol(1));
  assert(to_symbol(cdr(c2)) == 1);


  printf("\n-------------------------------------------- Eq testing\n");

  eqtest("num", from_fixnum(0), from_fixnum(0), T);
  eqtest("num", from_fixnum(1), from_fixnum(1), T);
  eqtest("num", from_fixnum(0), from_fixnum(1), NIL);
  eqtest("num", from_fixnum(1), from_fixnum(0), NIL);

  eqtest("num", from_fixnum(FIXNUM_MAX), from_fixnum(FIXNUM_MAX), T);
  eqtest("num", from_fixnum(FIXNUM_MIN), from_fixnum(FIXNUM_MIN), T);
  eqtest("num", from_fixnum(FIXNUM_MIN), from_fixnum(FIXNUM_MAX), NIL);
  eqtest("num", from_fixnum(FIXNUM_MAX), from_fixnum(FIXNUM_MIN), NIL);

  eqtest("num", from_fixnum(0), INVALID, NIL);
  eqtest("num", from_fixnum(FIXNUM_MIN), INVALID, NIL);
  eqtest("num", from_fixnum(FIXNUM_MAX), INVALID, NIL);

  eqtest("invalid", INVALID, INVALID, T);
  eqtest("invalid", INVALID, T, NIL);
  eqtest("invalid", T, INVALID, NIL);


  eqtest("nil", NIL, NIL, T);
  eqtest("nil", NIL, INVALID, NIL);
  eqtest("nil", INVALID, NIL, NIL);

  eqtest("nil", NIL, cons(1, 2), NIL);
  eqtest("nil", cons(1, 2), NIL, NIL);
  eqtest("nil", cons(0,0), NIL, NIL);
  eqtest("nil", NIL, cons(0,0), NIL);

  if (heap->capacity < 30) {
    printf("Heap capacity (%d) too small to continue tests!\n", heap->capacity);
    exit(-1);
  }
  eqtest("cons",
	 cons(from_fixnum(1), from_fixnum(2)),
	 cons(from_fixnum(1),from_fixnum(2)),
	 NIL);

  eqtest("cons", cons(0,0), cons(0,0), NIL);
  eqtest("cons", INVALID, cons(INVALID, T), NIL);
  eqtest("cons", cons(NIL, T), INVALID, NIL);

  value_t ls1 = cons(0,0);
  value_t ls2 = cons(0,0);
  eqtest("cons", ls1, ls2, NIL);
  eqtest("cons", ls2, ls1, NIL);
  eqtest("cons", ls1, ls1, T);
  eqtest("cons", ls2, ls2, T);
  eqtest("cons", ls1, INVALID, NIL);
  

  printf("\n-------------------------------------------- Atom testing\n");

  printf("Reinitializing heap...\n");
  if (!((heap = new_heap(30)))) {
    printf("ERROR initializing heap!  Exiting...\n");
    exit(-1);
  }
  ENV = NIL;
  
  atomtest("invalid", INVALID, T);
  atomtest("nil", NIL, T);
  atomtest("fixnum", one, T);
  atomtest("fixnum", negative_nine, T);
  atomtest("fixnum", from_fixnum(FIXNUM_MAX), T);
  atomtest("fixnum", from_fixnum(FIXNUM_MIN), T);
  atomtest("fixnum", from_fixnum(0), T);
  atomtest("symbol", T, T);
  atomtest("symbol", from_symbol(999), T);
  atomtest("symbol", from_symbol(MAX_SYMBOL), T);
  atomtest("locative", from_locative(to_locative(0)), T);
  atomtest("cons", cons(NIL, NIL), NIL);
  

  printf("\n-------------------------------------------- Eval testing\n");

  ENV = extend_env1(NIL, NIL, NIL); /* bind NIL */
  ENV = extend_env1(ENV, T, T);	    /* bind T */

  /* INVALID is not eq to anything */
  evaltest("self evals", INVALID, INVALID, true, ENV);
  evaltest("self evals", INVALID, NIL, false, ENV);
  evaltest("self evals", INVALID, T, false, ENV);
  evaltest("self evals", NIL,     INVALID, false, ENV);
  evaltest("self evals", NIL,     NIL, true, ENV);
  evaltest("self evals", NIL,     T, false, ENV);
  evaltest("self evals", T,       INVALID, false, ENV);
  evaltest("self evals", T,       NIL, false, ENV);
  evaltest("self evals", T,       T, true, ENV);

  evaltest("self evals", from_fixnum(0), from_fixnum(0), true, ENV);
  evaltest("self evals", from_fixnum(to_symbol(T)), T, false, ENV);
  evaltest("self evals", from_fixnum(to_symbol(NIL)), NIL, false, ENV);
  evaltest("self evals", from_fixnum(to_symbol(quote)), quote, false, ENV);
  evaltest("self evals", NIL, from_fixnum(to_symbol(NIL)), false, ENV);

  /* Tests that leverage the environment */

  dump_env(ENV);

  printf("Looking up T ==> %s\n", tostring(env_lookup(ENV, T)));
  assert(env_lookup(ENV, T) == T);
  assert(env_lookup(ENV, NIL) == NIL);

  evaltest("eval with env", T, from_fixnum(to_symbol(T)), false, ENV);

  evaltest("eval with env", quote, from_fixnum(to_symbol(quote)), false, ENV);
  evaltest("eval with env", quote, from_fixnum(to_symbol(quote)), false, ENV);

  evaltest("eval with env", T, INVALID, false, ENV);
  evaltest("eval with env", INVALID, T, false, ENV);
  evaltest("eval with env", T, T, true, ENV); /* T bound now */

  assert(env_lookup(ENV, from_symbol(5)) == INVALID);
  printf("BINDING symbol 5\n");
  ENV = extend_env1(ENV, from_symbol(5), from_fixnum(111));
  dump_env(ENV);
  dump_heap(0, 15);
  printf("Looking up symbol 5 ==> %s\n", tostring(env_lookup(ENV, from_symbol(5))));
  assert(env_lookup(ENV, from_symbol(5)) == from_fixnum(111));
  

  printf("\n-------------------------------------------- Equal testing\n");
  
  printf("Reinitializing heap...\n");
  if (!((heap = new_heap(30)))) {
    printf("ERROR initializing heap!  Exiting...\n");
    exit(-1);
  }
  ENV = extend_env1(NIL, T, T);	/* bind T */

  equaltest(from_fixnum(1), from_fixnum(1), T);
  equaltest(from_fixnum(0), from_fixnum(1), NIL);
  equaltest(cons(from_fixnum(0), NIL), from_fixnum(0), NIL);
  equaltest(from_fixnum(0), cons(from_fixnum(0), NIL), NIL);
  equaltest(cons(from_fixnum(0), NIL),
	    cons(from_fixnum(0), NIL),
	    T);
  equaltest(cons(from_fixnum(0), cons(T, NIL)),
	    cons(from_fixnum(0), cons(T, NIL)),
	    T);
  equaltest(cons(T, NIL),
	    cons(T, NIL),
	    T);
  equaltest(cons(T, INVALID),
	    cons(T, INVALID),
	    T);
  equaltest(INVALID, INVALID, T);
  

  printf("\n-------------------------------------------- Apply testing\n");
  
  printf("Reinitializing heap...\n");
  if (!((heap = new_heap(100)))) {
    printf("ERROR initializing heap!  Exiting...\n");
    exit(-1);
  }
  ENV = extend_env1(NIL, T, T);	/* bind T */

  applytest(T, from_fixnum(1), INVALID, ENV);

  applytest(CONS, from_fixnum(1), INVALID, ENV);
  applytest(CONS, cons(from_fixnum(1), NIL), cons(from_fixnum(1), NIL), ENV);

  applytest(CAR, from_fixnum(1), INVALID, ENV);
  applytest(CAR, cons(from_fixnum(0), from_fixnum(1)), INVALID, ENV);
  applytest(CAR, cons(cons(from_fixnum(0), from_fixnum(1)), NIL), from_fixnum(0), ENV);

  applytest(CDR, from_fixnum(1), INVALID, ENV);
  applytest(CDR, cons(from_fixnum(0), from_fixnum(1)), INVALID, ENV);
  applytest(CDR, cons(cons(from_fixnum(0), from_fixnum(1)), NIL), from_fixnum(1), ENV);

  applytest(ATOM, from_fixnum(1), INVALID, ENV);
  applytest(ATOM, cons(from_fixnum(1), NIL), T, ENV);
  applytest(ATOM, cons(T, NIL), T, ENV);
  applytest(ATOM, cons(NIL, NIL), T, ENV);
  applytest(ATOM, INVALID, INVALID, ENV);
  applytest(ATOM, cons(INVALID, INVALID), T, ENV);
  applytest(ATOM, cons(QUOTE, NIL), T, ENV);
  applytest(ATOM, cons(NIL, NIL), T, ENV);

  applytest(EQ, from_fixnum(1), INVALID, ENV);
  applytest(EQ, cons(from_fixnum(1), NIL), NIL, ENV);
  applytest(EQ, cons(from_fixnum(1), cons(from_fixnum(1), NIL)), T, ENV);

  dump_env(ENV);

  symbol_t newsymbol_number = symbol_intern("newsymbol");
  assert(newsymbol_number == -1); /* symbol table should be full from prior tests */
  symbol_table = new_symbol_table(50);

  dump_symbol_table();
  newsymbol_number = symbol_intern("newsymbol");  
  value_t newsymbol = from_symbol(newsymbol_number);
  /*
    Result should be INVALID because it doesn't make sense to apply a
    symbol.  Rather, we should bind the symbol to a function (lambda
    or primitive), eval the symbol, and then pass the resulting value
    to 'apply'.
   */
  applytest(newsymbol, NIL, INVALID, ENV);

  /* bind newsymbol to car */
  ENV = extend_env1(ENV, newsymbol, CAR);
  dump_env(ENV);
  /* check that we can apply newsymbol, which is CAR*/
  applytest(eval(newsymbol, ENV),
	    cons(cons(from_fixnum(5), from_fixnum(6)), NIL),
	    from_fixnum(5),
	    ENV);
  /* rebind (create a new binding for) newsymbol to cdr */
  ENV = extend_env1(ENV, newsymbol, CDR);
  dump_env(ENV);
  /* check that we can apply newsymbol, which is CDR */
  applytest(eval(newsymbol, ENV),
	    cons(cons(from_fixnum(5), from_fixnum(6)), NIL),
	    from_fixnum(6),
	    ENV);

  printf("\n-------------------------------------------- Eval testing with forms\n");

  printf("Reinitializing heap...\n");
  if (!((heap = new_heap(500)))) {
    printf("ERROR initializing heap!  Exiting...\n");
    exit(-1);
  }
  ENV = extend_env1(NIL, NIL, NIL); /* bind NIL */
  ENV = extend_env1(ENV, T, T);	    /* bind T */

  evaltest("(quote . 1)", cons(QUOTE, from_fixnum(1)), INVALID, true, ENV);
  evaltest("(quote . t)", cons(QUOTE, T), INVALID, true, ENV);

  evaltest("(quote 1)",
	   cons(QUOTE, cons(from_fixnum(1), NIL)),
	   from_fixnum(1),
	   true,
	   ENV);
  
  evaltest("(quote (T . NIL))",
	   cons(QUOTE, cons(cons(T, NIL), NIL)),
	   cons(T, NIL),
	   true,
	   ENV);

  evaltest("(quote (NIL . NIL))",
	   cons(QUOTE, cons(cons(NIL, NIL), NIL)),
	   cons(NIL, NIL),
	   true,
	   ENV);

  evaltest("(quote quote)",
	   cons(QUOTE, cons(QUOTE, NIL)),
	   QUOTE,
	   true,
	   ENV);

  evaltest("(quote (car eq))",
	   cons(QUOTE, cons(cons(CAR, cons(EQ, NIL)), NIL)),
	   cons(CAR, cons(EQ, NIL)),
	   true,
	   ENV);

  evaltest("(quote 1)",
	   cons(QUOTE, cons(from_fixnum(1), NIL)),
	   from_fixnum(1),
	   true,
	   ENV);

  evaltest("(quote newsymbol)",
	   cons(QUOTE, cons(newsymbol, NIL)),
	   newsymbol,
	   true,
	   ENV);

  evaltest("(if . 1)",
	   cons(IF, from_fixnum(1)),
	   INVALID,
	   true,
	   ENV);

  evaltest("(if 1)",
	   cons(IF, cons(from_fixnum(1), NIL)),
	   NIL,
	   true,
	   ENV);
  
  evaltest("(if 1 T)",
	   cons(IF, cons(from_fixnum(1), cons(T, NIL))),
	   T,
	   true,
	   ENV);

  /* Evaluate (newsymbol) where newsymbol is not bound */
  evaltest("newsymbol not bound", cons(newsymbol, NIL), INVALID, true, ENV);
  ENV = extend_env1(ENV, newsymbol, CDR);
  dump_env(ENV);
  printf("NOTE: newsymbol is now bound to CDR\n");
  evaltest("(newsymbol . NIL)", cons(newsymbol, NIL), NIL, true, ENV);
  evaltest("(newsymbol 1)",
	   cons(newsymbol, cons(from_fixnum(1), NIL)),
	   INVALID,
	   true,
	   ENV);
  evaltest("(newsymbol '(1 . 2))", 
	   cons(newsymbol, cons(cons(QUOTE, cons(cons(from_fixnum(1), from_fixnum(2)), NIL)), NIL)), 
	   from_fixnum(2), 
	   true, 
	   ENV); 

  symbol_t newcar = symbol_intern("newcar");
  symbol_t newcdr = symbol_intern("newcdr");
  assert(newcar != -1);
  assert(newcdr != -1);
  ENV = pairlis(ENV,
		cons(from_symbol(newcar), cons(from_symbol(newcdr), NIL)),
		cons(CAR,    cons(CDR, NIL)));
		   
  printf("This env should have newcar and newcdr bound:\n");
  dump_env(ENV);
  assert(env_lookup(ENV, from_symbol(newcar)) == CAR);
  assert(env_lookup(ENV, from_symbol(newcdr)) == CDR);

  value_t temp_env = pairlis(ENV, NIL, NIL);
  assert(temp_env == ENV);
		   
  ENV = pairlis(ENV,
		cons(from_symbol(newcar), NIL),
		cons(T, NIL)); 
		   
  printf("This env should have ANOTHER newcar bound (to t):\n");
  dump_env(ENV);
  assert(env_lookup(ENV, from_symbol(newcar)) == T);

  value_t tempENV = pairlis(ENV,
			    cons(from_symbol(newcar), cons(from_symbol(newcdr), NIL)),
			    /* value list too short, returns INVALID */
			    cons(QUOTE, NIL));
  assert(tempENV == INVALID);
		   
  ENV = pairlis(ENV,
		cons(from_symbol(newcar), cons(from_symbol(newcdr), NIL)),
		cons(QUOTE, cons(NIL, NIL)));

  printf("This env should have newcar bound to QUOTE, and newcdr bound to NIL:\n");
  dump_env(ENV);
  assert(env_lookup(ENV, from_symbol(newcar)) == QUOTE);
  assert(env_lookup(ENV, from_symbol(newcdr)) == NIL);
  

  printf("\n-------------------------------------------- Native listn function\n");

  value_t ls = listn(0);
  assert(ls == NIL);
  ls = listn(1, from_fixnum(1));
  printf("ls is %s\n", tostring(ls));
  assert(equal(ls, cons(from_fixnum(1), NIL)));
  ls = listn(2, T, T);
  printf("ls is %s\n", tostring(ls));
  assert(equal(ls, cons(T, cons(T, NIL))));
  ls = listn(2, T, NIL);
  printf("ls is %s\n", tostring(ls));
  assert(equal(ls, cons(T, cons(NIL, NIL))));
  ls = listn(3, T, T, T);
  printf("ls is %s\n", tostring(ls));
  assert(equal(ls, cons(T, cons(T, cons(T, NIL)))));
  ls = listn(3, from_fixnum(1), from_fixnum(2), from_fixnum(3));
  printf("ls is %s\n", tostring(ls));
  assert(equal(ls, cons(from_fixnum(1), cons(from_fixnum(2), cons(from_fixnum(3), NIL)))));

  bool ok = nreverse(&ls);
  printf("nreverse(ls) returned %s\n", ok ? "ok" : "error");
  printf("ls is %s\n", tostring(ls));
  assert(equal(ls, cons(from_fixnum(3), cons(from_fixnum(2), cons(from_fixnum(1), NIL)))));
  
  printf("a plain old pair looks like: %s\n", tostring(cons(from_fixnum(-1), from_fixnum(-2))));
  ls = listn(1, cons(from_fixnum(100), from_fixnum(101)));
  printf("listn of a single pair looks like: %s\n", tostring(ls));


  printf("\n-------------------------------------------- Native map and for_each functions\n");

  value_t cars = map(car, NIL);
  assert(cars == NIL);
  cars = map(car, listn(1, cons(from_fixnum(1), from_fixnum(2))));
  assert(equal(cars, listn(1, from_fixnum(1))));
  ls = listn(2,
	     cons(from_fixnum(1), from_fixnum(2)),
	     cons(from_fixnum(3), from_fixnum(4)));
  cars = map(car, ls);
  printf("cars: %s\n", tostring(cars));
  assert(equal(cars, listn(2, from_fixnum(1), from_fixnum(3))));

  ok = for_each(displayln, ls);


  printf("\n-------------------------------------------- Eval testing with lambda\n");

  printf("Reinitializing heap...\n");
  if (!((heap = new_heap(300)))) {
    printf("ERROR initializing heap!  Exiting...\n");
    exit(-1);
  }
  ENV = extend_env1(NIL, T, T);	/* bind T */
  ENV = extend_env1(ENV, from_symbol(symbol_intern("i")), from_fixnum(0));

  value_t proc = eval(listn(1,LAMBDA), ENV);
  assert(proc == INVALID);	/* proc has NIL body */

  proc = eval(listn(3, LAMBDA, NIL, T), ENV);
  printf("proc looks like this: %s\n", tostring(proc));
  assert(TYPE_OF(proc) == TYPE_CONS);

  symbol_t n = symbol_intern("n");
  assert(n != -1);
  proc = eval(listn(3, LAMBDA, listn(1, from_symbol(n)), T), ENV);
  printf("proc looks like this: %s\n", tostring(proc));
  assert(TYPE_OF(proc) == TYPE_CONS);
  assert(equal(car(cdr(proc)), cons(from_symbol(n), NIL)));
  dump_env(car(cdr(cdr(cdr(proc)))));

  proc = eval(listn(3,
		    LAMBDA,
		    listn(1, from_symbol(n)),
		    listn(2, CAR, listn(2, QUOTE, cons(from_fixnum(1), from_fixnum(2))))),
	      ENV);
  printf("proc looks like this: %s\n", tostring(proc));

  proc = eval(listn(3,
		    LAMBDA,
		    NIL,
		    listn(2, CAR, listn(2, QUOTE, cons(from_fixnum(1), from_fixnum(2))))),
	      ENV);
  printf("proc looks like this: %s\n", tostring(proc));

  value_t newproc = eval(proc, ENV);
  printf("result of eval on proc is: %s\n", tostring(newproc));

  value_t result = apply(proc, NIL, ENV);
  printf("result of applying proc to no args is: %s\n", tostring(result));
  assert(equal(result, from_fixnum(1)));

  /* Function of no args */
  proc = eval(listn(3,
		    LAMBDA,
		    NIL,
		    listn(3, CONS, from_fixnum(99), from_fixnum(100))),
	      ENV);
  printf("proc looks like this: %s\n", tostring(proc));
  result = apply(proc, NIL, ENV);
  printf("result of applying proc to no args is: %s\n", tostring(result));
  assert(equal(result, cons(from_fixnum(99), from_fixnum(100))));
  
  /* Function of one arg */
  symbol_t p = symbol_intern("p");
  proc = eval(listn(3,
		    LAMBDA,
		    listn(1, from_symbol(p)),
		    listn(2, CAR, from_symbol(p))),
	      ENV);
  printf("proc looks like this: %s\n", tostring(proc));
  result = apply(proc, NIL, ENV);
  printf("result of applying proc to no args is: %s\n", tostring(result));
  assert(eq(result, INVALID));
  result = apply(proc, listn(1, cons(from_fixnum(66), NIL)), ENV);
  printf("result of applying proc to a pair is: %s\n", tostring(result));
  assert(eq(result, from_fixnum(66)));

  /* Function of two args */
  symbol_t a = symbol_intern("a");
  symbol_t b = symbol_intern("b");
  proc = eval(listn(3,
		    LAMBDA,
		    listn(2, from_symbol(a), from_symbol(b)),
		    listn(3, CONS, from_symbol(a), from_symbol(b))),
	      ENV);
  printf("proc looks like this: %s\n", tostring(proc));
  result = apply(proc, NIL, ENV);
  printf("result of applying proc to no args is: %s\n", tostring(result));
  assert(eq(result, INVALID));
  result = apply(proc, listn(2, from_fixnum(17), from_fixnum(19)), ENV);
  printf("result of applying proc to two args is: %s\n", tostring(result));
  assert(equal(result, cons(from_fixnum(17), from_fixnum(19))));

  /* Function of two args, body has two forms */
  proc = eval(listn(4,
		    LAMBDA,
		    listn(2, from_symbol(a), from_symbol(b)),
		    from_fixnum(10000), /* eval'd during execution, result ignored */
		    listn(3, CONS, from_symbol(a), from_symbol(b))),
	      ENV);
  printf("proc looks like this: %s\n", tostring(proc));
  result = apply(proc, listn(2, from_fixnum(17), from_fixnum(19)), ENV);
  printf("result of applying proc to two args is: %s\n", tostring(result));
  assert(equal(result, cons(from_fixnum(17), from_fixnum(19))));

  /* A lambda expression can be evaluated twice, yielding the same value both times */
  value_t proc1 = eval(listn(3,
			     LAMBDA,
			     listn(1, from_symbol(n)),
			     listn(1, from_fixnum(42))),
		       ENV);
  printf("proc1 looks like this: %s\n", tostring(proc1));
  value_t proc2 = eval(proc1, ENV);
  printf("proc1 looks like this: %s\n", tostring(proc2));
  assert(eq(proc1, proc2));

  puts("Done.");
}
