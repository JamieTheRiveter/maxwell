/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  maxwell.h                                                               */
/*                                                                          */
/*  AUTHORS: Jamie A. Jennings                                              */

#ifndef maxwell_h
#define maxwell_h

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>		/* for varargs */
#include <stdlib.h>		/* for malloc/free */

typedef int64_t value_t;
#define _LSHIFT(v, amt) (((uint64_t) (v)) << (amt))
#define _RSHIFT(v, amt) (((int64_t) (v)) >> (amt))

/* 56 bits data + 6 bits type tag + 2 bits GC state. */
/* GC may or may not be implemented in the future. */
#define N_VALUE_BITS   (56)
#define N_TYPE_BITS    (6)
#define N_GC_BITS      (2)

#define MASK_FOR_GC    (_LSHIFT(1L, N_GC_BITS)-1)
#define MASK_FOR_TYPE  (_LSHIFT(_LSHIFT(1L, N_TYPE_BITS)-1, N_GC_BITS))
#define MASK_FOR_VALUE (_LSHIFT(_LSHIFT(1L, N_VALUE_BITS)-1, (N_TYPE_BITS + N_GC_BITS)))

#define GC_OF(v)    ((v) & MASK_FOR_GC)
#define TYPE_OF(v)  (_RSHIFT((v) & MASK_FOR_TYPE, N_GC_BITS))
#define VALUE_OF(v) (_RSHIFT((v) & MASK_FOR_VALUE, N_TYPE_BITS + N_GC_BITS))

#define _VALUESHIFT(v) (_LSHIFT((v), N_TYPE_BITS + N_GC_BITS))
#define _TYPESHIFT(v)  (_LSHIFT((v), N_GC_BITS))
#define TO_VALUE_T(v, typetag) \
  ((value_t) (_VALUESHIFT((v)) | (_TYPESHIFT(typetag) & MASK_FOR_TYPE)))

#define _TYPE_LIST(X)			                         \
       /* Cons and symbol are the two elementary types */        \
       X(TYPE_CONS,      "cons")                                 \
       X(TYPE_SYMBOL,    "symbol")	                         \
       /* The invalid type is for error propagation */           \
       X(TYPE_INVALID,   "invalid")	                         \
       /* The locative type helps us implement the store */      \
       X(TYPE_LOCATIVE,  "locative")	                         \
       /* A fixnum type lets us write better example programs */ \
       X(TYPE_FIXNUM,    "fixnum")	                         \
       /* Primitive functions let us operate on fixnums */       \
       X(TYPE_PRIMITIVE, "primitive")
  
#define _FIRST(a, b) a,
typedef enum {
    _TYPE_LIST(_FIRST)
    TYPE_COUNT
} value_types;
#undef _FIRST

#define _ELEMENTARY_SYMBOLS(X)			\
       X(_CONS, CONS, "cons")			\
       X(_CAR, CAR, "car")			\
       X(_CDR, CDR, "cdr")			\
       X(_QUOTE, QUOTE, "quote")		\
       X(_IF, IF, "if")				\
       X(_ATOM, ATOM, "atom")			\
       X(_EQ, EQ, "eq")				\
       /* Symbols below are not elementary. */	\
       X(_T, T, "t")				\
       X(_NIL, NIL, "nil")			\
       X(_LAMBDA, LAMBDA, "lambda")		\
       X(_PROC, PROC, "proc")

#define _FIRST(a, b, c) a,
typedef enum { 
    _ELEMENTARY_SYMBOLS(_FIRST) 
    ELEMENTARY_SYMBOLS_COUNT 
} elementary_symbols; 
#undef _FIRST

#define _DECLARE(number, name, namestring) const value_t name; 
_ELEMENTARY_SYMBOLS(_DECLARE) 
#undef _DECLARE

const value_t INVALID;
const int64_t FIXNUM_MAX;
const int64_t FIXNUM_MIN;

const char *typename (int typetag);

/*
  A locative is an index into the heap.  The heap is an array of
  values.  Since a cons cell consists of two locatives, it occupies
  two consecutive locations in the store.  The locative for the cell
  points to the car.
*/

typedef int32_t locative_t;
#define ERR_LOCATIVE (-1)
#define MAX_LOCATIVE (INT32_MAX)

typedef struct {
  value_t    *store;
  locative_t  capacity;
  locative_t  next;
} heap_t;

heap_t *heap;

typedef int32_t symbol_t;	/* index into symbol table */
#define ERR_SYMBOL (-1)
#define MAX_SYMBOL (INT32_MAX)

typedef struct {
  char const **names;
  symbol_t capacity;
  symbol_t next;
} symbol_table_t;

symbol_table_t *symbol_table;

typedef int16_t primitive_t;
#define ERR_PRIMITIVE (-1)
#define MAX_PRIMITIVE (INT16_MAX)
// TODO: Define an enumeration of primitive functions

/* ----------------------------------------------------------------------------- */
/* API                                                                           */
/* ----------------------------------------------------------------------------- */

primitive_t to_primitive (value_t val);

bool fits_fixnum (int64_t i);
int64_t to_fixnum (value_t val);
value_t from_fixnum (int64_t i);

symbol_t to_symbol (value_t val);
value_t from_symbol (symbol_t index);

locative_t to_locative (value_t val);
value_t from_locative (locative_t loc);

locative_t to_cons (value_t val);
value_t from_cons (locative_t loc);

value_t cons (value_t car, value_t cdr);
value_t car  (value_t pair);
value_t cdr  (value_t pair);

bool eq    (value_t a, value_t b);
bool atom  (value_t v);
bool equal (value_t a, value_t b);
bool consp (value_t thing);
bool listp (value_t thing);
bool symbolp (value_t thing);

value_t eval  (value_t exp, value_t env);
value_t apply (value_t operator, value_t operands, value_t env);

heap_t     *new_heap (locative_t size);
locative_t  allocate (int n);
locative_t  store_value (value_t val);
locative_t  store_value_at (locative_t loc, value_t val);
value_t     load_value (locative_t loc);

const char     *symbol_name (symbol_t number);
symbol_t        symbol_intern (const char *name);
symbol_table_t *new_symbol_table (symbol_t size);

bool    nreverse (value_t *ls);
bool    for_each (value_t (*proc)(value_t), value_t list);
value_t map      (value_t (*proc)(value_t), value_t list);
value_t listn    (int n, ...);

value_t newline (void);
value_t display (value_t item);
value_t displayln (value_t item);

value_t env_lookup  (value_t env, value_t sym);
value_t extend_env1 (value_t env, value_t sym, value_t v);
value_t pairlis     (value_t env, value_t namelist, value_t valuelist);

/* ----------------------------------------------------------------------------- */
/* Printing                                                                      */
/* ----------------------------------------------------------------------------- */

char *tostring (value_t thing);          /* Caller must free the returned string */
void print_value_as_value (value_t thing);

void dump_heap (locative_t start, locative_t end);
void dump_symbol_table (void);
void dump_env (value_t env);
void print_value_tags (value_t v);

/* ----------------------------------------------------------------------------- */
/* Logging and tracing                                                           */
/* ----------------------------------------------------------------------------- */

bool CONTINUE_AFTER_PANIC;
void print_trace(const char *filename, int lineno, ...);
void print_vtrace(const char *filename, int lineno, const char *msg, value_t value);

/* TRACE and PANIC take args like those to printf() */
#define TRACE(...) do {					\
    print_trace(__FILE__, __LINE__, __VA_ARGS__);	\
  } while (0)

#define PANIC(...) do {					\
    print_trace(__FILE__, __LINE__, __VA_ARGS__);	\
    fflush(stderr);					\
    if (!CONTINUE_AFTER_PANIC) exit(-255);		\
  } while (0)

/* VTRACE takes a string (message) and ONE value_t arg */
#define VTRACE(msg, value) do {				\
    print_vtrace(__FILE__, __LINE__, msg, value);	\
  } while (0)

/* ----------------------------------------------------------------------------- */
/* Disqualifiers and consistency checks */
/* ----------------------------------------------------------------------------- */

#if __SIZEOF_POINTER__ != 8
#error Size of pointer not 64 bits and code tested only on 64 bit platforms
#endif

#if (N_VALUE_BITS + N_TYPE_BITS + N_GC_BITS) != 64
#error Value size and tag size do not add up to 64 bits
#endif

#endif
