# Maxwell

## Everyone sing along

> Bang, bang, Maxwell\'s silver hammer  
> Came down upon her head  
> Bang, bang, Maxwell\'s silver hammer  
> Made sure that she was dead  
&#8212; <cite>_Maxwell\'s Silver Hammer_, Lennon-McCartney</cite>

## Credit Alan Kay with the analogy

> Lisp [is] the Maxwell\'s Equations of Software!  
— <cite> 
Alan Kay
 ([ACM Programming Languages, Volume 2 Number 9](https://queue.acm.org/detail.cfm?id=1039523) 2004)
</cite>

On page 13 of the [Lisp 1.5 Programmer's
Manual](LISP_1.5_Programmers_Manual.pdf) are defined `eval` and `apply`, which
together not only specify the Lisp language (as it existed at the time), but
also _implemented it_.

![Definitions of eval and apply from Lisp 1.5](equations-of-software.jpg "The
Maxwell's Equations of software")


## The essence of Lisp

To get a working Lisp, you merely needed to implement `eval` and `apply` as
shown, which requires first implementing seven elementary operators on which
they depend:

* `cons` to build a pair, which requires allocating memory
* `car` to access the first item in a pair
* `cdr` to access the second item in a pair
* `quote` to suppress evaluation of what follows, treating it literally
* `cond` (or the simpler version, `if`) for conditional evaluation
* `atom` to test whether a value is not a pair
* `eq` for the most essential kind of equality testing

A unique symbol should be returned by `eq` to indicate _false_ (i.e. that its
arguments are not the same).  Traditionally, this is the symbol `NIL`.  By
convention, a chain of pairs ending in `NIL` is called a _list_.  There is
nothing more to `NIL` than there is to any so-called _sentinel_ value -- it need
only be different from everything else.

Lisp programs are expressions, called _forms_ which are evaluated to produce
values.  A form may be a _symbol_, a _construct_ (or _compound form_) created
with `cons`, or a self-evaluating object (like numbers, should we decide to have
numbers in our Lisp).

Once the seven elementary operators are implemented, you can add the keystone of
the glorious arch that is Lisp, `lambda`.  This is not an elementary operator,
yet it holds everything up.

The symbol `lambda` is but an agreement between Lisp and the programmer.  When
the programmer starts a form with `lambda`, Lisp deigns to consider the form a
function (or more accurately, a _procedure_).  

I write a `lambda` followed by a _parameter list_ (a list of symbols) and a
_procedure body_ (some additional forms) and Lisp responds, "when you are ready
to `apply` this procedure, I'll bind the parameters to their arguments and
evaluate the body".

## The store and the environment


## Maxwell is a deviant

### Lexical (static) scope

In one crucial way, the code implemented here deviates semantically from Lisp
1.5.  We implement lexical scope, rather than the original Lisp's dynamic
scope, for a couple of reasons.

* Lexical scope is _good_.  We can read the text of a program and understand
  more of it than we could if we were unable to trace the binding of each name. 
* Because we _can_.  We extend just a little the contract between the
  interpreter and the programmer, and alter just a little the definitions of
  `eval` and `apply`.

We add one new clause to the contract between the programmer and the
interpreter, stating that the symbol `proc`, like `lambda`, marks a procedure.
Though `proc` marks the result of evaluating a `lambda` form.

In Lisp 1.5, `apply` understands an operator when it is elementary and when it
is one of two special forms, one of which is `lambda`.  The body of the lambda
form is evaluated in the current environment, giving the semantics of dynamic
scope. 

With the change to lexical scope, a `lambda` form is no longer something we can
apply.  It must be evaluated first, where it is turned into a `proc`.  This is a
minor operation, in which the initial symbol changes from `lambda` to `proc`,
and the current environment is added to the procedure definition.

Whereas a `lambda` form holds a list of parameter names, and a list of body
forms, a `proc` form holds those same items plus the lexical environment.
Notice that a function written with `lambda` is represented as a list, and so is
an evaluated function, marked with `proc`.  We have introduced no new elementary
operators, no new primitives, nothing but a new symbol, `proc`, that we have
declared to have, like `lambda`, a special meaning in `eval` and `apply`.

### Replacement for LABEL

Another deviation is `label`.
