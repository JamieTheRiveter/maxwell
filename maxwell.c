/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  maxwell.c                                                               */
/*                                                                          */
/*  AUTHORS: Jamie A. Jennings                                              */

/* GENERAL TODO ITEMS:
   - strcmp
   - strdup
*/

#include "maxwell.h"
#include <stdio.h>		/* for logging/tracing */
#include <string.h>
#include <assert.h>

void print_trace(const char *filename, int lineno, ...) {
  char *fmt;
  va_list ap;
  va_start(ap, lineno);
  fmt = va_arg(ap, char *);
  fprintf(stderr, "MX: %s:%d ", filename, lineno);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputc('\n', stderr);
  fflush(stderr);
}

void print_vtrace(const char *filename, int lineno, const char *msg, value_t value) {
  char *representation = tostring(value);
  fprintf(stderr, "MX: %s:%d %s: %s\n", filename, lineno, msg, representation);
  free(representation);
  fflush(stderr);
}

#define defconst(number, name, ignore)				\
  const value_t name = (value_t) TO_VALUE_T(number, TYPE_SYMBOL);

_ELEMENTARY_SYMBOLS(defconst)

#define _THIRD(a, b, c) c,
static const char *const ELEMENTARY_SYMBOL_NAMES[] = { 
  _ELEMENTARY_SYMBOLS(_THIRD) 
}; 
#undef _THIRD

const value_t INVALID = (value_t) TO_VALUE_T(0L, TYPE_INVALID);
const int64_t FIXNUM_MAX = ((((value_t) 1) << (N_VALUE_BITS - 1)) - 1);
const int64_t FIXNUM_MIN = (- (((value_t) 1) << (N_VALUE_BITS - 1)));

#define _SECOND(a, b) b,
static const char *const VALUE_TYPENAMES[] = {
    _TYPE_LIST(_SECOND)
};
#undef _SECOND

const char *typename (int typetag) {
  return ((typetag >= 0) && (typetag < TYPE_COUNT))
    ? VALUE_TYPENAMES[(typetag)]
    : "{TYPE TAG OUT OF RANGE}";
}

#define TORNIL(b) (b ? T : NIL)

primitive_t to_primitive (value_t val) {
  return ((primitive_t) VALUE_OF(val));
}

bool fits_fixnum (int64_t i) {
  return ((i >= FIXNUM_MIN) && (i <= FIXNUM_MAX));
}

int64_t to_fixnum (value_t val) {
  return VALUE_OF(val);
}

value_t from_fixnum (int64_t i) {
  if (fits_fixnum(i)) 
    return TO_VALUE_T(i, TYPE_FIXNUM);
  TRACE("fixnum value out of range: %lld", i);
  return INVALID;
}

static bool fits_symbol (symbol_t index) {
  return (index >= 0) && (index <= MAX_SYMBOL);
}

symbol_t to_symbol (value_t val) {
  return ((symbol_t) VALUE_OF(val));
}

value_t from_symbol (symbol_t index) {
  if (fits_symbol(index))
    return TO_VALUE_T((value_t) index, TYPE_SYMBOL);
  TRACE("symbol number out of range: %d", index);
  return INVALID;
}

static bool fits_locative (locative_t loc) {
  return (loc >= 0) && (loc <= MAX_LOCATIVE);
}

locative_t to_locative (value_t val) {
  return ((locative_t) VALUE_OF(val));
}

value_t from_locative (locative_t loc) {
  if (fits_locative(loc))
    return TO_VALUE_T((value_t) loc, TYPE_LOCATIVE);
  TRACE("locative out of range: %lld", loc);
  return INVALID;
}

locative_t to_cons (value_t val) {
  return ((locative_t) VALUE_OF(val));
}

value_t from_cons (locative_t loc) {
  return TO_VALUE_T((value_t) loc, TYPE_CONS);
}

bool eq (value_t a, value_t b) {
  return (a == b);
}

value_t cons (value_t car, value_t cdr) {
  locative_t start = allocate(2);
  if (start == ERR_LOCATIVE) {
    TRACE("out of memory");
    return INVALID;
  }
  locative_t car_locative = store_value_at(start, car);
  if (car_locative == ERR_LOCATIVE) {
    TRACE("store failed -- trying to store CAR value at location %d", start);
    return INVALID;
  }
  locative_t cdr_locative = store_value_at(++start, cdr);
  if (cdr_locative == ERR_LOCATIVE) {
    TRACE("store failed -- trying to store CDR value at location %d", start);
    return INVALID;
  }
  return from_cons(car_locative);
}  

static locative_t car_loc (value_t pair) {
  return to_locative(pair);
}

static locative_t cdr_loc (value_t pair) {
  return to_locative(pair) + 1;
}

value_t car (value_t pair) {
  if (pair == NIL) return NIL;
  if (consp(pair)) return load_value(car_loc(pair));
  VTRACE("type error: cannot take CAR of", pair);
  return INVALID;
}

value_t cdr (value_t pair) {
  if (pair == NIL) return NIL;
  if (consp(pair)) return load_value(cdr_loc(pair));
  VTRACE("type error: cannot take CDR of", pair);
  return INVALID;
}

bool atom (value_t v) {
  return (TYPE_OF(v) != TYPE_CONS);
}

bool consp (value_t thing) {
  return (TYPE_OF(thing) == TYPE_CONS);
}

bool listp (value_t thing) {
  return (consp(thing) || (thing == NIL));
}

bool symbolp (value_t thing) {
  return (TYPE_OF(thing) == TYPE_SYMBOL);
}

/* TODO: Make this iterative to avoid using C stack */
bool equal (value_t a, value_t b) {
  if (atom(a) || atom(b)) return eq(a, b);
  return equal(car(a), car(b)) && equal(cdr(a), cdr(b));
}

/* TODO: Rewrite with iteration to avoid using C stack */
static value_t evlis (value_t exps, value_t env) {
  if (!listp(exps)) {
    VTRACE("bad argument to evlis; expected list of exps, received", exps);
    return INVALID;
  }
  if (exps == NIL) return NIL;
  return cons(eval(car(exps), env), evlis(cdr(exps), env));
}

static value_t construct_procedure (value_t exp, value_t env) {
  assert(eq(car(exp), LAMBDA));
  value_t parmlist = car(cdr(exp));
  value_t body = cdr(cdr(exp));
  if (body == NIL) {
    VTRACE("procedure has nil body", exp);
    return INVALID;
  }
  return listn(4, PROC, parmlist, body, env);
}

static value_t progn (value_t forms, value_t env) {
  if (!listp(forms)) {
    PANIC("procedure body not a list of forms", tostring(forms));
    return INVALID;
  }
  if (!listp(env)) {
    PANIC("procedure environment not a list", tostring(env));
    return INVALID;
  }
  value_t result = INVALID;
  while (forms != NIL) {
    result = eval(car(forms), env);
    forms = cdr(forms);
  }
  return result;
}

value_t apply (value_t operator, value_t operands, value_t env) {
  if (operator == INVALID) {
    TRACE("invalid operator to APPLY");
    return INVALID;
  }
  if (!listp(operands)) {
    VTRACE("invalid operands to APPLY (expected a list)", operands);
    return INVALID;
  }
  if (atom(operator)) {
    switch (operator) {
    case CAR: return car(car(operands));
    case CDR: return cdr(car(operands));
    case CONS: return cons(car(operands), car(cdr(operands)));
    case ATOM: return TORNIL(atom(car(car(operands))));
    case EQ: return TORNIL(eq(car(operands), car(cdr(operands))));
    default:
      if (symbolp(operator))
	return apply(eval(operator, env), operands, env);
      else {
	VTRACE("invalid operator to APPLY", operator);
	return INVALID;
      }	
    }
  }
  assert(consp(operator));
  switch (car(operator)) {
  case PROC: {
    value_t paramlist = car(cdr(operator));
    value_t bodyforms = car(cdr(cdr(operator)));
    value_t stored_env = car(cdr(cdr(cdr(operator))));
    value_t procedure_env = pairlis(stored_env, paramlist, operands);
    if (procedure_env == INVALID) return INVALID;
    return progn(bodyforms, procedure_env);
  }
    //  case LABEL: {
    //  }
  default:
    VTRACE("invalid operator to APPLY", operator);
    return INVALID;
  } /* switch on car(operator) */
}

value_t eval (value_t exp, value_t env) {
  if (atom(exp)) {
    if (symbolp(exp)) return env_lookup(env, exp);
    /* Other atoms evaluate to themselves */
    return exp;
  }
  assert(consp(exp));
  switch (car(exp)) {
  case QUOTE:
    return car(cdr(exp));
  case IF: {
    value_t test_exp = car(cdr(exp));
    value_t then_exp = car(cdr(cdr(exp)));
    value_t else_exp = car(cdr(cdr(cdr(exp))));
    if (eval(test_exp, env) != NIL) return eval(then_exp, env);
    else return eval(else_exp, env);
  }
  case LAMBDA:
    /*
      Here we deviate from the original Lisp by saving the lexical
      environment with the procedure definition.  Lisp 1.5 had no
      special clause for LAMBDA in 'eval'.
    */
    return construct_procedure(exp, env);
  case PROC:
    return exp;
  default:
    return apply(car(exp), evlis(cdr(exp), env), env);
  }
}

/* ----------------------------------------------------------------------------- */
/* STORAGE                                                                       */
/* ----------------------------------------------------------------------------- */

heap_t *new_heap (locative_t size) {
  if ((size < 30) || (size > MAX_LOCATIVE)) {
    PANIC("requested heap size out of range: %d", size);
    return NULL;
  }
  heap_t *newheap = malloc(sizeof(heap_t));
  if (!newheap) goto eomem;
  newheap->store = malloc(sizeof(value_t) * (size_t) size);
  if (!newheap->store) {
    free(newheap);
    goto eomem;
  }
  newheap->capacity = size;
  newheap->next = 0;
  return newheap;

 eomem:
  PANIC("malloc failed");
  return NULL;
}

value_t load_value (locative_t loc) {
  if ((loc >= 0) && (loc < heap->capacity))
    return heap->store[loc];
  PANIC("memory fault (load from invalid address %ld)", loc);
  return INVALID;
}

locative_t store_value_at (locative_t loc, value_t val) {
  if ((loc >= 0) && (loc < heap->capacity)) {
    heap->store[loc] = val;
    return loc;
  }
  PANIC("memory fault (store to invalid address %ld)", loc);
  return ERR_LOCATIVE;
}

locative_t allocate (int n) {
  assert(heap->capacity > 0);
  assert(heap->next >= 0);
  assert(heap->next <= heap->capacity);

  if (n < 1) {
    PANIC("allocator received request for %d cells", n);
    return ERR_LOCATIVE;
  }

  if ((heap->next + n) > heap->capacity) {
    PANIC("out of memory: (%d/%d cells in use, asking for %d in total)",
	   heap->next,
	   heap->capacity,
	   heap->next + n);
    return ERR_LOCATIVE;
  }

  locative_t start = heap->next;
  heap->next += n;
  return start;
}

locative_t store_value (value_t val) { 
  locative_t loc = allocate(1);
  if (loc == ERR_LOCATIVE) return ERR_LOCATIVE;
  return store_value_at(loc, val);
} 

const char *symbol_name (symbol_t number) {
  if ((number < 0) || (number >= symbol_table->next))
    return NULL;
  return symbol_table->names[number];
}

/*
  Intern a symbol name.  Returns existing number if name already
  exists, else creates a new entry.  TODO: Use a hash table?
*/
symbol_t symbol_intern (const char *name) {
  symbol_t n;
  for (n = 0; n < symbol_table->next; n++)
    /* FIXME: strcmp is a security exposure */
    if (strcmp(name, symbol_table->names[n])==0)
      return n;
  assert(n == symbol_table->next);
  if (symbol_table->next == symbol_table->capacity)
    return ERR_LOCATIVE;
  /* FIXME: strdup is a security exposure and possible leak */
  symbol_table->names[n] = strdup(name);
  symbol_table->next++;
  return n;
}

static symbol_table_t *new_empty_symbol_table (symbol_t size) {
  if ((size < ELEMENTARY_SYMBOLS_COUNT) || (size > MAX_SYMBOL)) {
    printf("Invalid symbol table size: %d\n", size);
    return NULL;
  }
  symbol_table_t *new_symtab = malloc(sizeof(symbol_table_t));
  if (!new_symtab) {
    return NULL;
  }
  new_symtab->capacity = size;
  new_symtab->next = 0;
  new_symtab->names = malloc(sizeof(char *) * (size_t) size);
  if (!new_symtab->names) {
    free(new_symtab);
    return NULL;
  }
  return new_symtab;
}

static void initialize_symbol_table (symbol_table_t *new_symtab) {
  if (!(new_symtab
	&& (new_symtab->next == 0)
	&& (new_symtab->capacity >= ELEMENTARY_SYMBOLS_COUNT))) {
    PANIC("Cannot initialize symbol table\n");
    return;
  }
  for (int i=0; i < ELEMENTARY_SYMBOLS_COUNT; i++)
    new_symtab->names[i] = ELEMENTARY_SYMBOL_NAMES[i];
  new_symtab->next = ELEMENTARY_SYMBOLS_COUNT;
}

symbol_table_t *new_symbol_table (symbol_t size) {
  symbol_table_t *new_symtab = new_empty_symbol_table(size);
  if (new_symtab) {
    initialize_symbol_table(new_symtab);
    return new_symtab;
  }
  return NULL;
}

/* ----------------------------------------------------------------------------- */
/* UTILITIES                                                                     */
/* ----------------------------------------------------------------------------- */

bool nreverse (value_t *ls) {
  if (!listp(*ls)) return false; /* assumes a proper list */
  if (*ls == NIL) return true;
  if (cdr(*ls) == NIL) return true;

  value_t prev = NIL;
  value_t current = *ls;
  value_t next;
  while (current != NIL) {
    assert(consp(current));
    next = cdr(current);
    store_value_at(cdr_loc(current), prev);
    prev = current;
    current = next;
  }
  *ls = prev;
  return true;
}
    
value_t listn (int n, ...) {
  if (n < 0) {
    TRACE("invalid size in listn: %d", n);
    return INVALID;
  }
  value_t *values = malloc(sizeof(value_t) * (size_t) n);
  va_list ap;
  va_start(ap, n);
  for (int i = 0; i < n; i++) values[i] = va_arg(ap, value_t);
  va_end(ap);
  value_t ls = NIL;
  for (int i = n-1; i >= 0; i--) ls = cons(values[i], ls);
  free(values);
  return ls;
}
  
/* TODO: Rewrite with iteration to avoid using C stack */
value_t map (value_t (*proc)(value_t), value_t list) {
  if (!listp(list)) {
    VTRACE("type error: map second arg not a list", list);
    return INVALID;
  }
  if (list == NIL) return NIL;
  value_t newvalue = proc(car(list));
  if (newvalue == INVALID) {
    VTRACE("in map, proc arg returned invalid for element", car(list));
    return INVALID;
  }
  return cons(newvalue, map(proc, cdr(list)));
}

/* TODO: Rewrite with iteration to avoid using C stack */
bool for_each (value_t (*proc)(value_t), value_t list) {
  if (!listp(list)) {
    VTRACE("in for_each, second arg not a list", list);
    return false;
  }
  if (list == NIL) return NIL;
  if (proc(car(list)) == INVALID) {
    VTRACE("in for_each, proc arg returned invalid for element", car(list));
    return false;
  }
  return for_each(proc, cdr(list));
}

/* ----------------------------------------------------------------------------- */
/* ENVIRONMENTS                                                                  */
/* ----------------------------------------------------------------------------- */

value_t env_lookup (value_t env, value_t sym) {
  if (!listp(env)) {
    PANIC("lookup received an invalid env (expected a list): %s", tostring(env));
    return INVALID;
  }
  while (env != NIL) {
    value_t entry = car(env);
    if (eq(car(entry), sym)) return cdr(entry);
    env = cdr(env);
  }
  VTRACE("unbound symbol", sym);
  return INVALID;
}

value_t extend_env1 (value_t env, value_t sym, value_t v) {
  if (!listp(env)) {
    VTRACE("invalid env (expected a list)", env);
    return INVALID;
  }
  if (!symbolp(sym)) {
    VTRACE("expected a symbol, received", sym);
    return INVALID;
  }
  if (listp(env)) {
    return cons(cons(sym, v), env);
  }
  PANIC("lookup received an invalid env (expected a list): %s", tostring(env));
  return INVALID;
}

/* We'd probably call this 'extend_env' today. */
value_t pairlis (value_t env, value_t namelist, value_t valuelist) {
  if (!listp(namelist)) {
    PANIC("type error: invalid name list: %s", tostring(namelist));
    return INVALID;
  }
  if (!listp(valuelist)) {
    PANIC("type error: invalid value list: %s", tostring(valuelist));
    return INVALID;
  }
  while ((namelist != NIL) && (valuelist != NIL)) {
    env = extend_env1(env, car(namelist), car(valuelist));
    if (env == INVALID) return INVALID;
    namelist = cdr(namelist);
    valuelist = cdr(valuelist);
  }
  if ((namelist != NIL) || (valuelist != NIL)) {
    VTRACE("wrong number of arguments", listn(2, namelist, valuelist));
    return INVALID;
  }
  return env;
}

/* ----------------------------------------------------------------------------- */
/* PRINTING                                                                      */
/* ----------------------------------------------------------------------------- */

static char *procedure_tostring (value_t proc) {
  char *s;
  assert(eq(car(proc), LAMBDA));
  value_t paramlist = car(cdr(proc));
  value_t bodyforms = car(cdr(cdr(proc)));
  value_t env = car(cdr(cdr(cdr(proc))));
  assert(listp(env));
  char *lam = tostring(LAMBDA);
  char *parms = tostring(paramlist);
  char *body = tostring(bodyforms);
  asprintf(&s, "#{(%s %s %s) #{env %d}}",
	   lam, parms, body, /* Body will print as a LIST of forms */
	   to_locative(env));
  free(lam); free(parms); free(body);
  return s;
}

/* NOTE: Caller must free the returned string */
char *tostring (value_t thing) {
  char *s;
  switch (TYPE_OF(thing)) {
  case TYPE_INVALID:
    return strdup("#{invalid}");
  case TYPE_FIXNUM:
    asprintf(&s, "%lld", to_fixnum(thing));
    return s;
  case TYPE_SYMBOL: {
    const char *name = symbol_name(to_symbol(thing));
    if (name) return strdup(name);
    else asprintf(&s, "#{symbol #%d}", to_symbol(thing));
    return s;
  }
  case TYPE_PRIMITIVE:
    /* HACK UNTIL WE HAVE A TABLE OF PRIMITIVES */
    asprintf(&s, "#{primitive #%d}", to_primitive(thing));
    return s;
  case TYPE_CONS: {
    if (eq(car(thing), LAMBDA)) return procedure_tostring(thing);
    char *tmp;
    asprintf(&s, "(%s", tostring(car(thing)));
    thing = cdr(thing);
    if (consp(thing))
      while (consp(cdr(thing))) {
	tmp = s;
	asprintf(&s, "%s %s", tmp, tostring(car(thing)));
	free(tmp);
	thing = cdr(thing);
      }
    if (thing == NIL) {
      tmp = s;
      asprintf(&s, "%s)", tmp);
      free(tmp);
      return s;
    }
    if (consp(thing) && (cdr(thing) == NIL)) {
      tmp = s;
      asprintf(&s, "%s %s)", tmp, tostring(car(thing)));
      free(tmp);
      return s;
    }
    tmp = s;
    asprintf(&s, "%s . %s)", tmp, tostring(thing));
    free(tmp);
    return s;
  }
  case TYPE_LOCATIVE:
    asprintf(&s, "#{locative %d}", to_locative(thing));
    return s;
  default:
    asprintf(&s, "#{ERROR: unknown value type %lld}", TYPE_OF(thing));
    return s;
  }
}

void print_value_as_value (value_t thing) {
  switch (TYPE_OF(thing)) {
  case TYPE_INVALID:
    printf("#{invalid}"); break;
  case TYPE_FIXNUM:
    printf("#{fixnum %lld}", to_fixnum(thing)); break;
  case TYPE_SYMBOL:
    printf("#{symbol #%d}", to_symbol(thing)); break;
  case TYPE_PRIMITIVE:
    printf("#{primitive #%d}", to_primitive(thing)); break;
  case TYPE_CONS:
    printf("#{cons locative %d}", to_locative(thing)); break;
  case TYPE_LOCATIVE:
    printf("#{locative %d}", to_locative(thing)); break;
  default:
    printf("#{UNKNOWN: type is %lld}", TYPE_OF(thing));
  }
}

value_t newline (void) {
  puts("");
  return NIL;
}

value_t display (value_t item) {
  printf("%s", tostring(item));
  return NIL;
}

value_t displayln (value_t item) {
  display(item);
  newline();
  return NIL;
}

void dump_heap (locative_t start, locative_t end) {
  start = (start < 0) ? 0 : start;
  end = ((end < 0) || (end > heap->capacity)) ? heap->capacity : end;
  printf("HEAP: capacity %d, next free cell %d", heap->capacity, heap->next);
  for (; start < end; start++) {
    printf("\nheap[%d]: ", start);
    print_value_as_value(heap->store[start]);
  }
  puts("");
}

void dump_symbol_table (void) {
  printf("symbol_table: capacity %d, next free slot %d\n",
	 symbol_table->capacity, symbol_table->next);
  for (symbol_t i = 0; i < symbol_table->next; i++)
    printf("symbol %d: %s\n", i, symbol_table->names[i]);
}

void dump_env (value_t env) {
  printf("Contents of environment:\n");
  if (!listp(env)) {
    PANIC("Invalid type for environment: %s", typename(TYPE_OF(env)));
    return;
  }
  while (env != NIL) {
    value_t entry = car(env);
    value_t sym = car(entry);
    value_t value = cdr(entry);
    if (!symbolp(sym))
      PANIC("  Error ... key in env that is not a symbol: %s", tostring(sym));
    const char *name = symbol_name(to_symbol(sym));
    if (name)
      printf("  %s ==> %s\n", name, tostring(value));
    else
      printf("  #{symbol %d} ==> %s\n", to_symbol(sym), tostring(value));
    env = cdr(env);
  } /* loop through env */
}

void print_value_tags (value_t v) {
  printf("-----  Incoming word: %016x, ", (unsigned) v);
  printf("value: %014llX, type: %01llX, gc: %01llX\n",
	 (uint64_t) VALUE_OF(v),
	 (uint64_t) TYPE_OF(v),
	 (uint64_t) GC_OF(v));
}

